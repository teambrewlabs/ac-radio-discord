# Froggy Chair Radio

Froggy Chair Radio is a Discord bot that plays Animal Crossing music based on the time of day, made at [Oxford Hack 2020](https://oxfordhack.co.uk).

> Back in the year 2005, prior to the success of the fabled Lo-fi Hip Hop Study Mix, chilled evenings were relegated to only the chosen few. Then Nintendo released Animal Crossing: Wild World.
>
> Contributing to the game's laid-back atmosphere was the dynamic soundtrack that constantly evolved based on the time of day. Suddenly, Nintendo DS owners everywhere had the ability to chill placed within their own grasp. We wanted to re-live those heady days of kicking back on the froggy chair, all within the comfort of our proprietary voice chat software of choice, Discord.

## Usage

You can add Froggy Chair Radio to your Discord server by inviting it by opening [this link](https://discord.com/api/oauth2/authorize?client_id=777207075641098260&permissions=3155968&scope=bot)!

The bot responds to the following commands:

- `!play` - start playing the music for the current hour
- `!play [game] [hour]` - start playing music for the specified `game` (must be supported in the config file) and/or specified `hour` (must be an integer between `0` and `23`, e.g. `!play 15`)
- `!nowplaying` - announce the currently playing song
- `!skip` - skip the current track
- `!stop` - stop the music
- `!louder` - give the music some **bass** (WARNING: loud!) (disabled for now)
- `!help` - display a help message in chat

## Installation

To run the bot yourself, you must create a Discord bot at the [development portal](https://discordapp.com/developers/applications/) and download the hourly Animal Crossing music (available [here](https://downloads.khinsider.com/game-soundtracks/album/animal-crossing-wild-world)).
You will also need to have a recent version of Node.js.

Clone the repo and install prerequisites:
```
git clone https://gitlab.com/teambrewlabs/ac-radio-discord.git
cd ac-radio-discord
npm install
```

Next you will need to edit `config.json` so that `token` is set to your bot's token.
For each game you have music for, specify a `music_prefixes` entry pointing to the folder where the music is stored, and a `music_names` entry with the long-form name of the game.
An example of this is in `config.json`.
The music files must be in the format `hour.wav` and (optionally) `hour_intro.wav`, where `hour` is 2 digits representing the hour - for example, `16.wav` and `16_intro.wav` for 4pm.

Now we can run the bot!
```
node index.js
```

## Running as a systemd service

To run as a systemd service, isolated from the rest of your system, you can make use of `froggy-chair-radio.service`.

First, copy it to `/etc/systemd/system/froggy-chair-radio.service`, and reload with:

```
systemctl daemon-reload
```

Then create the `froggy-chair-radio` user and directory:

```
groupadd -r froggy-chair-radio
useradd -r -g froggy-chair-radio -d "/var/lib/froggy-chair-radio" -s "/bin/bash" froggy-chair-radio
mkdir -p /var/lib/froggy-chair-radio
chown froggy-chair-radio:froggy-chair-radio /var/lib/froggy-chair-radio
```

Now navigate to `froggy-chair-radio`'s home and follow the instructions from the [Installation](#Installation):

```
su froggy-chair-radio
cd
```

Configure the token, acquire the music, etc.

Finally, get your systemd service running and restarting on reboot with:

```
systemctl enable --now froggy-chair-radio.service
```

### If something goes wrong
Find out why the service is failing with `systemctl status froggy-chair-radio.service` or `journalctl -xeu froggy-chair-radio.service`.  Good luck from here.

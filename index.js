const Discord = require("discord.js");
const { prefix, token, music_prefixes, music_names } = require("./config.json");
const fs = require("fs");

const client = new Discord.Client();

const queue = new Map();

client.once("ready", () => {
    console.log("Ready!");
});

client.once("reconnecting", () => {
    console.log("Reconnecting!");
});

client.once("disconnect", () => {
    console.log("Disconnect!");
});

client.on("message", async message => {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const serverQueue = queue.get(message.guild.id);

    if (message.content.startsWith(`${prefix}play`)) {
        execute(message, serverQueue);
        return;
    } else if (message.content.startsWith(`${prefix}nowplaying`)) {
        nowPlaying(message, serverQueue);
        return;
    //} else if (message.content.startsWith(`${prefix}louder`)) {
    //    death(message, serverQueue);
    //    return;
    } else if (message.content.startsWith(`${prefix}skip`)) {
        skip(message, serverQueue);
        return;
    } else if (message.content.startsWith(`${prefix}stop`)) {
        stop(message, serverQueue);
        return;
    } else if (message.content.startsWith(`${prefix}help`)) {
        message.channel.send({
            embed: {
                color: 11361238,
                title: "Available commands:",
                fields: [
                    {
                        name: "Command",
                        value: "play [game] [hour]\n\nstop\nskip\nnowplaying\nhelp",
                        inline: true
                    },
                    {
                        name: "Result",
                        value: "Play music for the current hour, optionally specifying game (ww/nl) and hour (24 hour format)\nStop playing\nSkip to the next hour\nDisplay the currently playing track\nSend this message",
                        inline: true
                    }
                ]
            }
        });
    }
});

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function get_time_hours() {
    var d = new Date();
    return d.getHours();
}

function get_filename(game, time=null) {
    var prefix = music_prefixes[game];

    // Strip trailing slash if exists
    if (prefix.charAt(prefix.length-1) == '/') {
        prefix = prefix.slice(0, -1);
    }

    // Get current time if not specified
    time = pad((time !== null) ? time : get_time_hours(), 2);

    const intro_name = `${prefix}/${time}_intro.wav`;
    const loop_name = `${prefix}/${time}.wav`;

    return [
        {
            'filename': fs.existsSync(intro_name) ? intro_name : null
        },
        {
            'filename': fs.existsSync(loop_name) ? loop_name : null
        }
    ];
}

// fades out or in over specified number of seconds, with resolution many volume steps per second
async function fade(serverQueue, out=true, seconds=3, resolution=5) {
    const volume_start = out ? serverQueue.volume / 5 : 0;
    const volume_end = out ? 0 : serverQueue.volume / 5;

    const dispatcher = serverQueue.connection.dispatcher;

    dispatcher.setVolumeLogarithmic(volume_start);
    for (let i = 1; i <= seconds * resolution; i++) {
        setTimeout(function() {
            const volume_diff = ((volume_end - volume_start) / (seconds * resolution)) * i;
            dispatcher.setVolumeLogarithmic(volume_start + volume_diff);
        }, (1000 / resolution) * i);
    }
}

async function execute(message, serverQueue) {
    const args = message.content.split(" ");

    var hour = null;
    var game = null;

    for (let i = 1; i < args.length; i++) {
        if (Object.keys(music_prefixes).includes(args[i])) {
            game = args[i];
        } else {
            hour_arg = parseInt(args[i])
            if (hour_arg >= 0 && hour_arg < 24) {
                hour = hour_arg
            } else {
                return message.channel.send(
                    "Argument " + args[i] + " not recognised! It should be a game (" + Object.keys(music_prefixes).join("/") + ") or hour."
                );
            }
        }
    }

    if (game === null) {
        game = Object.keys(music_prefixes)[Math.floor(Math.random() * Object.keys(music_prefixes).length)]
    }

    const voiceChannel = message.member.voice.channel;
    if (!voiceChannel)
        return message.channel.send(
            "You need to be in a voice channel to play music!"
        );
    const permissions = voiceChannel.permissionsFor(message.client.user);
    if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
        return message.channel.send(
            "I need the permissions to join and speak in your voice channel!"
        );
    }

    var queueConstruct = null;
    if (!serverQueue) {
        queueConstruct = {
            textChannel: message.channel,
            voiceChannel: voiceChannel,
            connection: null,
            volume: 5,
            playing: (hour !== null) ? hour : get_time_hours(),
            game: game,
            timeout: null,
            disconnectTimeout: null
        };

        queue.set(message.guild.id, queueConstruct);

    } else {
        queueConstruct = queue.get(message.guild.id);
        queueConstruct.playing = (hour !== null) ? hour : get_time_hours();
        queueConstruct.game = game;
    }

    try {
        var connection = await voiceChannel.join();
        queueConstruct.connection = connection;
        play(message.guild, hour, game);
        scheduleSkip(message.guild, queueConstruct);
        scheduleDisconnect(message.guild, queueConstruct);
        nowPlaying(message, queueConstruct);
    } catch (err) {
        console.log(err);
        queue.delete(message.guild.id);
        return message.channel.send(err);
    }
}

function death(message, serverQueue) {
    if (!message.member.voice.channel)
        return message.channel.send(
            "You have to be in a voice channel to turn up the volume!"
        );
    if (!serverQueue)
        return message.channel.send("There isn't any music playing right now!");

    serverQueue.connection.dispatcher.setVolumeLogarithmic(serverQueue.volume * 5);

    return message.channel.send(
        "Welp, you asked for it..."
    )
}

function scheduleSkip(guild, serverQueue) {
    targetHour = (serverQueue.playing + 1) % 24;
    var targetDate = new Date();
    targetDate.setHours(targetHour);
    targetDate.setMinutes(0);
    targetDate.setSeconds(0);
    targetDate.setMilliseconds(0);
    if ((targetDate.getTime() - new Date().getTime()) < 0) {
        targetDate.setDate(targetDate.getDate() + 1);
    }

    millis = targetDate.getTime() - new Date().getTime();

    serverQueue.timeout = setTimeout(function() {
        doSkip(guild, serverQueue);
    }, millis);
}

function skip(message, serverQueue) {
    if (!message.member.voice.channel)
        return message.channel.send(
            "You have to be in a voice channel to stop the music!"
        );
    if (!serverQueue)
        return message.channel.send("There is no song that I could skip!");

    clearTimeout(serverQueue.timeout);

    doSkip(message.guild, serverQueue);

    nowPlaying(message, serverQueue);
}

function doSkip(guild, serverQueue) {
    serverQueue.playing = (serverQueue.playing + 1) % 24;
    fade(serverQueue, true, 3);
    setTimeout(function() {
        if (serverQueue.connection.dispatcher !== null) {
            serverQueue.connection.dispatcher.end();
        }
        play(guild, null, serverQueue.game);
    }, 5000);

    scheduleSkip(guild, serverQueue);
}

function scheduleDisconnect(guild, serverQueue) {
    if (serverQueue.disconnectTimeout !== null) {
        clearTimeout(serverQueue.disconnectTimeout);
    }

    serverQueue.disconnectTimeout = setTimeout(function() {
        checkDisconnect(guild, serverQueue);
    }, 120000);
}

// disconnect if no users in the channel, reschedule this function otherwise
function checkDisconnect(guild, serverQueue) {
    if (serverQueue.voiceChannel.members.size <= 1) {
        serverQueue.playing = null;
        serverQueue.connection.dispatcher.end();
        try {
            serverQueue.voiceChannel.leave();
        } catch (err) {
            console.log(err);
        }
    } else {
        serverQueue.disconnectTimeout = setTimeout(function() {
            checkDisconnect(guild, serverQueue);
        }, 120000);
    }
}

function nowPlaying(message, serverQueue) {
    if (serverQueue.playing !== null) {
        const hour = serverQueue.playing;
        const game = music_names[serverQueue.game];
        const hourModTwelve = (hour % 12 == 0) ? 12 : hour % 12;
        const ampm = (hour >= 0 && hour <= 11) ? "AM" : "PM";
        return message.channel.send(
            `Now playing: ${hourModTwelve}${ampm} from ${game}`
        );
    } else {
        return message.channel.send(
            "No song is currently playing!"
        );
    }
}

function stop(message, serverQueue) {
    if (!message.member.voice.channel)
        return message.channel.send(
            "You have to be in a voice channel to stop the music!"
        );

    clearTimeout(serverQueue.timeout);
    clearTimeout(serverQueue.disconnectTimeout);

    serverQueue.playing = null;
    serverQueue.game = null;
    serverQueue.connection.dispatcher.end();
    try {
        message.member.voice.channel.leave();
    } catch (err) {
        console.log(err);
    }
}

// If hour === null, play according to the current `playing` value
// Else, repeat this hour indefinitely
function play(guild, hour=null, game=null, intro=true) {
    const serverQueue = queue.get(guild.id);

    const currentHour = hour === null ? serverQueue.playing : hour;

    const currentGame = game === null ? Object.keys(music_prefixes)[Math.floor(Math.random() * Object.keys(music_prefixes).length)] : game;

    var songs = get_filename(currentGame, currentHour);

    const songIntro = songs[0];
    const song = songs[1];

    const filename = (intro && songIntro.filename !== null) ? songIntro.filename : song.filename;

    const dispatcher = serverQueue.connection
        .play(filename)
        .on("finish", () => {
            if (serverQueue.playing == currentHour) {
                play(guild, hour, currentGame, false)
            }
        })
        .on("error", error => console.error(error));
        dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
        //console.log(`Playing file: ${filename}`);
}

client.login(token);
